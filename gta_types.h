#pragma once

#include <ostream>

#include "vector/vector_t.h"
#include "matrix/matrix_t.h"

template<typename CharT>
std::basic_ostream<CharT> &operator<<( std::basic_ostream<CharT> &out, const gta_types::vector_t &vec ){
	out << vec.fX << ',' << vec.fY << ',' << vec.fZ;
	return out;
}

template<typename CharT>
std::basic_ostream<CharT> &operator<<( std::basic_ostream<CharT> &out, const gta_types::matrix_t &mat ){
	for ( int i = 0; i < 16; ++i ) {
		out << mat.cell[i];
		if ( i != 15 ) out << ',';
	}
	return out;
}
