#include <iostream>

#include "../gta_types.h"

struct TestVector {
	float x, y, z;
};

int main() {
	gta_types::vector_t vec( 0, 1, 2 );
	std::cout << vec << std::endl;

	TestVector testVec = vec;
	std::cout << testVec.x << "," << testVec.y << "," << testVec.z << std::endl;

	vec = testVec;
	std::cout << vec << std::endl;

	return 0;
}
