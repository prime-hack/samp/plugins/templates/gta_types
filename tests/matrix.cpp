#include <iostream>

#include "../gta_types.h"

struct TestMatrix {
	float cell[16];
};

int main() {
	gta_types::matrix_t mat;
	std::cout << mat << std::endl;

	TestMatrix testMat = mat;
	for ( int i = 0; i < 16; ++i ) {
		std::cout << testMat.cell[i];
		if ( i != 15 ) std::cout << ",";
	}
	std::cout << std::endl;

	mat = testMat;
	std::cout << mat << std::endl;

	return 0;
}
