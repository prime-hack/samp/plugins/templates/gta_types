Base code style was declared in `.clang-format`

### Headers

Headers must by self-sufficient and contain `#pragma once`


### Includes

Include ordering:
- Pair header file (when exists)
- Empty line
- C headers
- Empty line
- C++ headers
- Empty line
- 3rd party headers
- Empty line
- Project headers