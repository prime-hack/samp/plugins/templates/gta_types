#include "matrix_t.h"

#include <cmath>

gta_types::matrix_t::matrix_t() {
	up = { 1.0, 0.0, 0.0 };
	right = { 0.0, 1.0, 0.0 };
	at = { 0.0, 0.0, 1.0 };
	pos = { 0.0, 0.0, 0.0 };

	flags = 0;
	pad_u = 0.0;
	pad_p = 0.0;
	pad_a = 0.0;
}

gta_types::matrix_t::matrix_t( const matrix_t &rhs ) {
	*this = rhs;
}

gta_types::matrix_t::matrix_t( matrix_t &&rhs ) {
	*this = rhs;
}

gta_types::matrix_t::type_vector &gta_types::matrix_t::get_right() {
	return *reinterpret_cast<type_vector *>( &right );
}

gta_types::matrix_t::type_vector &gta_types::matrix_t::get_up() {
	return *reinterpret_cast<type_vector *>( &up );
}

gta_types::matrix_t::type_vector &gta_types::matrix_t::get_at() {
	return *reinterpret_cast<type_vector *>( &at );
}

gta_types::matrix_t::type_vector &gta_types::matrix_t::get_front() {
	return get_at();
}

gta_types::matrix_t::type_vector &gta_types::matrix_t::get_pos() {
	return *reinterpret_cast<type_vector *>( &pos );
}

gta_types::matrix_t &gta_types::matrix_t::set_scale( float rightScale, float upScale, float atScale ) {
	right = { rightScale, 0.0f, 0.0f };
	up = { 0.0f, upScale, 0.0f };
	at = { 0.0f, 0.0f, atScale };
	return *this;
}

gta_types::matrix_t &gta_types::matrix_t::set_scale( float scale ) {
	return set_scale( scale, scale, scale );
}

gta_types::matrix_t &gta_types::matrix_t::set_unity() {
	return set_scale( 1.0f );
}

gta_types::matrix_t &gta_types::matrix_t::set_rotate_x( float rotate ) {
	right = { 1.0f, 0.0f, 0.0f };
	up = { 0.0f, std::cos( rotate ), std::sin( rotate ) };
	at = { 0.0f, -std::sin( rotate ), std::cos( rotate ) };
	return *this;
}

gta_types::matrix_t &gta_types::matrix_t::set_rotate_y( float rotate ) {
	right = { std::cos( rotate ), 0.0f, -std::sin( rotate ) };
	up = { 0.0f, 1.0f, 0.0f };
	at = { std::sin( rotate ), 0.0f, std::cos( rotate ) };
	return *this;
}

gta_types::matrix_t &gta_types::matrix_t::set_rotate_z( float rotate ) {
	right = { std::cos( rotate ), std::sin( rotate ), 0.0f };
	up = { -std::sin( rotate ), std::cos( rotate ), 0.0f };
	at = { 0.0f, 0.0f, 1.0f };
	return *this;
}

gta_types::matrix_t &gta_types::matrix_t::set_rotate( float x, float y, float z ) {
	right.x = std::cos( z ) * std::cos( y ) - ( std::sin( z ) * std::sin( x ) ) * std::sin( y );
	right.y = ( std::cos( z ) * std::sin( x ) ) * std::sin( y ) + std::sin( z ) * std::cos( y );
	right.z = -( std::sin( y ) * std::cos( x ) );
	up.x = -( std::sin( z ) * std::cos( x ) );
	up.y = std::cos( z ) * std::cos( x );
	up.z = std::sin( x );
	at.x = std::cos( z ) * std::sin( y ) + ( std::sin( z ) * std::sin( x ) ) * std::cos( y );
	at.y = std::sin( z ) * std::sin( y ) - ( std::cos( z ) * std::sin( x ) ) * std::cos( y );
	at.z = std::cos( y ) * std::cos( x );
	return *this;
}

gta_types::matrix_t::type_vector gta_types::matrix_t::prject( const type_vector &offset ) const {
	type_vector offsetVector;
	offsetVector.x = ( offset.x * right.x ) + ( offset.y * up.x ) + ( offset.z * at.x );
	offsetVector.x = ( offset.x * right.y ) + ( offset.y * up.y ) + ( offset.z * at.y );
	offsetVector.x = ( offset.x * right.z ) + ( offset.y * up.z ) + ( offset.z * at.z );
	return offsetVector;
}

float gta_types::matrix_t::angle_z() const {
	return std::atan2( -up.x, up.y );
}

gta_types::matrix_t &gta_types::matrix_t::invert() {
	type_vector vOldRight = right, vOldFront = at, vOldUp = up;
	right = { vOldRight.x, vOldFront.x, vOldUp.x };
	at = { vOldRight.y, vOldFront.y, vOldUp.y };
	up = { vOldRight.z, vOldFront.z, vOldUp.z };
	pos.x *= -1.0f;
	pos.y *= -1.0f;
	pos.z *= -1.0f;
	return *this;
}

gta_types::matrix_t gta_types::matrix_t::operator+( const matrix_t &rhs ) const {
	matrix_t matResult;
	matResult.right = { right.x + rhs.right.x, right.y + rhs.right.y, right.z + rhs.right.z };
	matResult.at = { at.x + rhs.at.x, at.y + rhs.at.y, at.z + rhs.at.z };
	matResult.up = { up.x + rhs.up.x, up.y + rhs.up.y, up.z + rhs.up.z };
	matResult.pos = { pos.x + rhs.pos.x, pos.y + rhs.pos.y, pos.z + rhs.pos.z };
	return matResult;
}

gta_types::matrix_t gta_types::matrix_t::operator-( const matrix_t &rhs ) const {
	matrix_t matResult;
	matResult.right = { right.x - rhs.right.x, right.y - rhs.right.y, right.z - rhs.right.z };
	matResult.at = { at.x - rhs.at.x, at.y - rhs.at.y, at.z - rhs.at.z };
	matResult.up = { up.x - rhs.up.x, up.y - rhs.up.y, up.z - rhs.up.z };
	matResult.pos = { pos.x - rhs.pos.x, pos.y - rhs.pos.y, pos.z - rhs.pos.z };
	return matResult;
}

gta_types::matrix_t gta_types::matrix_t::operator*( const matrix_t &rhs ) const {
	matrix_t matResult;
	matResult.right = ( *this ) * rhs.right;
	matResult.at = ( *this ) * rhs.at;
	matResult.up = ( *this ) * rhs.up;
	matResult.pos = ( *this ) * rhs.pos;
	return matResult;
}

gta_types::matrix_t &gta_types::matrix_t::operator=( const matrix_t &rhs ) {
	right = rhs.right;
	up = rhs.up;
	at = rhs.at;
	pos = rhs.pos;

	flags = 0;
	pad_u = 0.0;
	pad_a = 0.0;
	pad_p = 0.0;

	return *this;
}

gta_types::matrix_t &gta_types::matrix_t::operator=( matrix_t &&rhs ) {
	right = rhs.right;
	up = rhs.up;
	at = rhs.at;
	pos = rhs.pos;

	flags = rhs.flags;
	pad_u = rhs.pad_u;
	pad_a = rhs.pad_a;
	pad_p = rhs.pad_p;

	return *this;
}

gta_types::matrix_t gta_types::matrix_t::operator/( const matrix_t &rhs ) const {
	auto copy = rhs;
	return ( *this ) * copy.invert();
}

gta_types::matrix_t::type_vector gta_types::matrix_t::operator*( const type_vector &rhs ) const {
	return type_vector{ right.x * rhs.x + at.x * rhs.y + up.x * rhs.z,
					 right.y * rhs.x + at.y * rhs.y + up.y * rhs.z,
					 right.z * rhs.x + at.z * rhs.y + up.z * rhs.z };
}

gta_types::matrix_t &gta_types::matrix_t::operator+=( const matrix_t &rhs ) {
	*this = *this + rhs;
	return *this;
}

gta_types::matrix_t &gta_types::matrix_t::operator-=( const matrix_t &rhs ) {
	*this = *this - rhs;
	return *this;
}

gta_types::matrix_t &gta_types::matrix_t::operator*=( const matrix_t &rhs ) {
	*this = *this * rhs;
	return *this;
}

gta_types::matrix_t &gta_types::matrix_t::operator/=( const matrix_t &rhs ) {
	*this = *this / rhs;
	return *this;
}

gta_types::matrix_t gta_types::matrix_t::from_quaternion( float w, float x, float y, float z ) {
	matrix_t mat;

	auto SquarredQuaterW = w * w;
	auto SquarredQuaterX = x * x;
	auto SquarredQuaterY = y * y;
	auto SquarredQuaterZ = z * z;
	mat.right.x = SquarredQuaterX - SquarredQuaterY - SquarredQuaterZ + SquarredQuaterW;
	mat.up.y = SquarredQuaterY - SquarredQuaterX - SquarredQuaterZ + SquarredQuaterW;
	mat.at.z = SquarredQuaterZ - ( SquarredQuaterY + SquarredQuaterX ) + SquarredQuaterW;

	float multXY = x * y;
	float multWZ = w * z;
	mat.up.x = multWZ + multXY + multWZ + multXY;
	mat.right.y = multXY - multWZ + multXY - multWZ;

	float multXZ = x * z;
	float multWY = w * y;
	mat.at.x = multXZ - multWY + multXZ - multWY;
	mat.right.z = multWY + multXZ + multWY + multXZ;

	float multYZ = y * z;
	float multWX = w * x;
	mat.at.y = multWX + multYZ + multWX + multYZ;
	mat.up.z = multYZ - multWX + multYZ - multWX;

	return mat;
}
