#pragma once

#if __has_include( "vector/vector_t.h" )
#	include "vector/vector_t.h"
#endif

namespace gta_types {
	class matrix_t {
		struct vector {
			float x, y, z;
		};
		static_assert( sizeof( vector ) == sizeof( float ) * 3 );

#if __has_include( "vector/vector_t.h" )
		using type_vector = vector_t;
#else
		using type_vector = vector;
#endif

	public:
		union {
			union {
				float m[4][4];
				float cell[16];
			};
			struct {
				union {
					vector right;
					vector vRight;
				};
				union {
					float pad_r;
					unsigned int flags;
					unsigned int dwPadRoll;
				};
				union {
					vector up;
					vector vUp;
				};
				union {
					float pad_u;
					unsigned int dwPadWas;
				};
				union {
					vector at;
					vector front;
					vector vFront;
				};
				union {
					float pad_a;
					float pad_f;
					unsigned int dwPadDirection;
				};
				union {
					vector pos;
					vector vPos;
				};
				union {
					float pad_p;
					unsigned int dwPadPos;
				};
			};
		};

		explicit matrix_t();
		matrix_t( const matrix_t &rhs );
		matrix_t( matrix_t &&rhs );

		type_vector &get_right();
		type_vector &get_up();
		type_vector &get_at();
		type_vector &get_front();
		type_vector &get_pos();

		matrix_t &set_scale( float rightScale, float upScale, float atScale );
		matrix_t &set_scale( float scale );

		matrix_t &set_unity();

		matrix_t &set_rotate_x( float rotate );
		matrix_t &set_rotate_y( float rotate );
		matrix_t &set_rotate_z( float rotate );
		matrix_t &set_rotate( float x, float y, float z );

		type_vector prject( const type_vector &offset ) const;

		float angle_z() const;

		matrix_t &invert();

		matrix_t operator+( const matrix_t &rhs ) const;
		matrix_t operator-( const matrix_t &rhs ) const;
		matrix_t operator*( const matrix_t &rhs ) const;
		matrix_t operator/( const matrix_t &rhs ) const;

		type_vector operator*( const type_vector &rhs ) const;

		matrix_t &operator=( const matrix_t &rhs );
		matrix_t &operator=( matrix_t &&rhs );

		matrix_t &operator+=( const matrix_t &rhs );
		matrix_t &operator-=( const matrix_t &rhs );
		matrix_t &operator*=( const matrix_t &rhs );
		matrix_t &operator/=( const matrix_t &rhs );

		static matrix_t from_quaternion( float w, float x, float y, float z );

		template<typename T> operator T() {
			static_assert( sizeof( matrix_t ) == sizeof( T ), "matrix_t::operator T: Invompatible type size" );
			return *reinterpret_cast<T *>( this );
		}

		template<typename T> matrix_t &operator=( const T &rhs ) {
			static_assert( sizeof( matrix_t ) == sizeof( T ), "matrix_t::operator =<T>: Invompatible type size" );
			*this = *reinterpret_cast<const matrix_t *>( &rhs );
			return *this;
		}

		template<typename T> matrix_t &operator=( T &&rhs ) {
			static_assert( sizeof( matrix_t ) == sizeof( T ), "matrix_t::operator =<T&&>: Invompatible type size" );
			*this = *reinterpret_cast<const matrix_t *>( &rhs );
			return *this;
		}
	};
	static_assert( sizeof( matrix_t ) == sizeof( float ) * 16 );
} // namespace Types
