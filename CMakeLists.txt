cmake_minimum_required(VERSION 3.0.0)
project(gta_types VERSION 0.1.0)

if (CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME)
	set(IS_TOPLEVEL_PROJECT TRUE)
else ()
	set(IS_TOPLEVEL_PROJECT FALSE)
endif ()

if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
	if (NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS 8.0)
		set(CMAKE_CXX17_STANDARD_COMPILE_OPTION "-std=c++17")
		set(CMAKE_CXX17_EXTENSION_COMPILE_OPTION "-std=gnu++17")
	elseif (NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS 5.1)
		set(CMAKE_CXX17_STANDARD_COMPILE_OPTION "-std=c++1z")
		set(CMAKE_CXX17_EXTENSION_COMPILE_OPTION "-std=gnu++1z")
	endif ()
endif ()

option(BUILD_TESTS "Build tests [ON/OFF]" ${IS_TOPLEVEL_PROJECT})

include(CTest)
enable_testing()

include_directories("${CMAKE_CURRENT_SOURCE_DIR}")

add_library(gta_types STATIC
    vector/vector_t.cpp vector/vector_t.h
	matrix/matrix_t.cpp matrix/matrix_t.h

	gta_types.h gta_types_fwd.h
)

if (BUILD_TESTS)
    add_executable(gta_types_vector_test tests/vector.cpp)
    target_link_libraries(gta_types_vector_test gta_types)
	add_executable(gta_types_matrix_test tests/matrix.cpp)
	target_link_libraries(gta_types_matrix_test gta_types)
endif()

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)
