#include "vector_t.h"

#include <cmath>

gta_types::vector_t::vector_t( float X, float Y, float Z ) : X( X ), Y( Y ), Z( Z ) {}

void gta_types::vector_t::clear() {
	for ( auto &v : f ) v = 0.0f;
}

bool gta_types::vector_t::empty() const {
	return x == 0.0f && y == 0.0f && z == 0.0f;
}

float gta_types::vector_t::dot_product( const vector_t &rhs ) const {
	return x * rhs.x + y * rhs.y + z * rhs.z;
}

float gta_types::vector_t::sqr_length() const {
	return dot_product( *this );
}

float gta_types::vector_t::length() const {
	return std::sqrt( sqr_length() );
}

void gta_types::vector_t::cross_product( const vector_t &rhs ) {
	float _fX = fX, _fY = fY, _fZ = fZ;
	fX = _fY * rhs.fZ - rhs.fY * _fZ;
	fY = _fZ * rhs.fX - rhs.fZ * _fX;
	fZ = _fX * rhs.fY - rhs.fX * _fY;
}

gta_types::vector_t &gta_types::vector_t::operator=( const vector_t &rhs ) {
	x = rhs.x;
	y = rhs.y;
	z = rhs.z;
	return *this;
}
gta_types::vector_t &gta_types::vector_t::operator=( vector_t &&rhs ) noexcept {
	x = rhs.x;
	y = rhs.y;
	z = rhs.z;
	return *this;
}

gta_types::vector_t gta_types::vector_t::operator+( const vector_t &rhs ) const {
	return vector_t( fX + rhs.fX, fY + rhs.fY, fZ + rhs.fZ );
}
gta_types::vector_t gta_types::vector_t::operator-( const vector_t &rhs ) const {
	return vector_t( fX - rhs.fX, fY - rhs.fY, fZ - rhs.fZ );
}
gta_types::vector_t gta_types::vector_t::operator*( const vector_t &rhs ) const {
	return vector_t( fX * rhs.fX, fY * rhs.fY, fZ * rhs.fZ );
}
gta_types::vector_t gta_types::vector_t::operator*( float rhs ) const {
	return vector_t( fX * rhs, fY * rhs, fZ * rhs );
}
gta_types::vector_t gta_types::vector_t::operator/( const vector_t &rhs ) const {
	return vector_t( fX / rhs.fX, fY / rhs.fY, fZ / rhs.fZ );
}
gta_types::vector_t gta_types::vector_t::operator/( float rhs ) const {
	return vector_t( fX / rhs, fY / rhs, fZ / rhs );
}
gta_types::vector_t gta_types::vector_t::operator-() const {
	return vector_t( -fX, -fY, -fZ );
}

gta_types::vector_t &gta_types::vector_t::operator+=( const vector_t &rhs ) {
	return operator=( operator+( rhs ) );
}
gta_types::vector_t &gta_types::vector_t::operator-=( const vector_t &rhs ) {
	return operator=( operator-( rhs ) );
}
gta_types::vector_t &gta_types::vector_t::operator*=( const vector_t &rhs ) {
	return operator=( operator*( rhs ) );
}
gta_types::vector_t &gta_types::vector_t::operator*=( float rhs ) {
	return operator=( operator*( rhs ) );
}
gta_types::vector_t &gta_types::vector_t::operator/=( const vector_t &rhs ) {
	return operator=( operator/( rhs ) );
}
gta_types::vector_t &gta_types::vector_t::operator/=( float rhs ) {
	return operator=( operator/( rhs ) );
}

bool gta_types::vector_t::is_normalized() const {
	return fX >= -1.0f && fX <= 1.0f && fY >= -1.0f && fY <= 1.0f && fZ >= -1.0f && fZ <= 1.0f;
}

gta_types::vector_t &gta_types::vector_t::normalize() {
	if ( empty() ) return *this;
	return operator/=( length() );
}

float gta_types::vector_t::angle() const {
	return -std::atan2( y, -x );
}

float gta_types::vector_t::dist( const vector_t &lhs, const vector_t &rhs ) {
	return ( lhs - rhs ).length();
}
float gta_types::vector_t::angle( const vector_t &lhs, const vector_t &rhs ) {
	float kx = rhs.X - lhs.X;
	float ky = rhs.Y - lhs.Y;

	if ( kx == 0 ) kx = 0.00001f;
	float t = kx / ky;
	if ( t < 0 ) t = -t;

	auto a = 180.0 * std::atan( t ) / 3.14159265358979323846;

	if ( ( kx <= 0 ) && ( ky <= 0 ) )
		a = 180.0 - a;
	else if ( ( kx >= 0 ) && ( ky >= 0 ) )
		a = 359.99999 - a;
	else if ( ( kx >= 0 ) && ( ky <= 0 ) )
		a = -( 180.0 - a );

	return ( a * 3.14159265358979323846 ) / 180.0;
}
