#pragma once

namespace gta_types {
	class vector_t {
	public:
		union {
			struct {
				float fX, fY, fZ;
			};
			struct {
				float X, Y, Z;
			};
			struct {
				float x, y, z;
			};
			float f[3];
		};

		explicit vector_t( float X = 0.0f, float Y = 0.0f, float Z = 0.0f );
		template<typename T> vector_t( const T &rhs ) { operator=<T>( rhs ); }
		template<typename T> vector_t( T &&rhs ) { operator=<T>( rhs ); }

		void clear();

		[[nodiscard]] bool empty() const;

		[[nodiscard]] float dot_product( const vector_t &rhs ) const;

		[[nodiscard]] float sqr_length() const;

		[[nodiscard]] float length() const;

		void cross_product( const vector_t &rhs );

		vector_t &operator=( const vector_t &rhs );
		vector_t &operator=( vector_t &&rhs ) noexcept;

		vector_t operator+( const vector_t &rhs ) const;
		vector_t operator-( const vector_t &rhs ) const;
		vector_t operator*( const vector_t &rhs ) const;
		vector_t operator*( float rhs ) const;
		vector_t operator/( const vector_t &rhs ) const;
		vector_t operator/( float rhs ) const;
		vector_t operator-() const;

		vector_t &operator+=( const vector_t &rhs );
		vector_t &operator-=( const vector_t &rhs );
		vector_t &operator*=( const vector_t &rhs );
		vector_t &operator*=( float rhs );
		vector_t &operator/=( const vector_t &rhs );
		vector_t &operator/=( float rhs );

		[[nodiscard]] bool is_normalized() const;

		vector_t &normalize();

		[[nodiscard]] float angle() const;

		[[nodiscard]] static float dist( const vector_t &lhs, const vector_t &rhs );
		[[nodiscard]] static float angle( const vector_t &lhs, const vector_t &rhs );

		template<typename T> operator T() {
			static_assert( sizeof( vector_t ) == sizeof( T ), "vector_t::operator T: Invompatible type size" );
			return *reinterpret_cast<T *>( this );
		}

		template<typename T> vector_t &operator=( const T &rhs ) {
			static_assert( sizeof( vector_t ) == sizeof( T ), "vector_t::operator =<T>: Invompatible type size" );
			*this = *reinterpret_cast<const vector_t *>( &rhs );
			return *this;
		}

		template<typename T> vector_t &operator=( T &&rhs ) {
			static_assert( sizeof( vector_t ) == sizeof( T ), "vector_t::operator =<T&&>: Invompatible type size" );
			*this = *reinterpret_cast<const vector_t *>( &rhs );
			return *this;
		}
	};
	static_assert( sizeof( vector_t ) == sizeof( float ) * 3 ); // prevent virtual methods :)
} // namespace Types
